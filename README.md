# OpenVSCode on Google Cloud Run
## Terraform code to bring up OpenVSCode on Google Cloud Run

# About
This will run [OpenVSCode](https://github.com/gitpod-io/openvscode-server) in a browser in [Google Cloud Run](https://cloud.google.com/run). Google Cloud Run will create a new container of OpenVSCode each time you access the persistent url. You can clone remote repositories, edit your code, and push back your changes. NO DATA WILL BE SAVED! If you close the session the file system will throw away all the changes. Be sure to commit and push your changes back to the git server.

# Security
There is no authentication configured. You can review the Cloud Run Documentation on [how to add end-user authentication](https://cloud.google.com/run/docs/authenticating/end-users). 

# Step to setup
1. Create a new project on Google Cloud Platform
1. Create a new Service Account in this project for terraform.
1. Enable the project for Container Registry & Cloud Run API.
1. Check for the latest container image on [Docker Hub](https://hub.docker.com/r/gitpod/openvscode-server/tags?page=1&ordering=last_updated) and update `push_img_to_gcp.sh`
1. Update `terraform.tfvars`
1. `terraform init`
1. `terraform plan`
2. `terraform apply`