location       = "us-central1"
name           = "gitpod"
project        = "gitpod-cloud-run"
image_name     = "us.gcr.io/gitpod-cloud-run/gitpod/openvscode-server:1.60.2"
container_port = 3000