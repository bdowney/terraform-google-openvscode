#!/usr/bin/env bash

# https://hub.docker.com/r/gitpod/openvscode-server/tags?page=1&ordering=last_updated
IMAGE="gitpod/openvscode-server:1.60.2"
GCP_PROJECT="gitpod-cloud-run"
GCP_REGION="us.gcr.io"
GCP_IMAGE="$GCP_REGION/$GCP_PROJECT/$IMAGE"

docker image pull $IMAGE
docker image tag $IMAGE $GCP_IMAGE
docker push $GCP_IMAGE

echo "$GCP_IMAGE"