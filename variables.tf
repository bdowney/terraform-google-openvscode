variable "name" {
  description = "Application name."
  type        = string
}

variable "location" {
  description = "Google location where resources are to be created."
  type        = string
}

variable "project" {
  description = "Google project ID."
  type        = string
}

variable "image_name" {
  description = "Docker image (i.e. us.gcr.io/project_name/docker_image:latest)."
  type        = string
}

variable "container_port" {
  description = "TCP Port container is listening and serving web traffic."
  type        = number
  default     = 80
}

variable "container_concurrency" {
  description = "Max number of connections per container instance."
  type        = number
  default     = 80 # Max per Cloud Run Documentation
}

variable "vpc_connector" {
  description = "Serverless VPC access connector."
  type        = string
  default     = ""
}
